import logging
import multiprocessing

from flask import Flask, render_template, request
from flask.json import jsonify
from flask_socketio import SocketIO

from streamreplayserver.util.get_streams import get_streams
from streamreplayserver.util.logging import setup_loggers

app = Flask(__name__)

multiprocessing.set_start_method("spawn")

setup_loggers()

logging.captureWarnings(True)
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

werkzeug_log = logging.getLogger("werkzeug")
werkzeug_log.setLevel(logging.INFO)

engineio_logger = logging.getLogger("engineio_logger")
engineio_logger.setLevel(logging.WARNING)

sio = SocketIO(
    app,
    async_mode="gevent",
    engineio_logger=engineio_logger,
)


@app.route("/")
def serve_index():
    return render_template("index.jinja2", streams=get_streams())


@app.route("/control/<channel>/<track>")
def serve_control(channel, track):
    hostname_base = request.host.split(":")[0]

    return render_template(
        "control.jinja2", channel=channel.lower(), track=track.lower(), hostname_base=hostname_base
    )


@app.route("/watch/<channel>/<track>")
def serve_watch(channel, track):
    hostname_base = request.host.split(":")[0]

    return render_template(
        "watch.jinja2", channel=channel.lower(), track=track.lower(), hostname_base=hostname_base
    )


@app.route("/api/v1/control/<channel>/<track>/<action>/<value>", methods=["POST"])
def serve_api_control(channel, track, action, value):
    sio.emit(
        "video_control",
        {"channel": channel.lower(), "track": track.lower(), "action": action.lower(), "value": value.lower()},
        namespace="/websocket",
    )

    return "", 200


@app.route("/api/v1/streams")
def serve_api_streams():
    return jsonify(get_streams())


@sio.on("connect", namespace="/websocket")
def sio_connect():
    logger.info("socket_io connect")


@sio.on("disconnect", namespace="/websocket")
def sio_disconnect():
    logger.info("socket_io disconnect")


logger.info("Server is now running.")
