import requests
from bs4 import BeautifulSoup


def get_streams():
    stream_info = {}

    rtmp_stats_request = requests.get("http://localhost/stats").text

    rtmp_stats = BeautifulSoup(rtmp_stats_request, features="lxml-xml")

    for application in rtmp_stats.find_all("application"):
        if application.find("name").text == "livereplay":
            for stream in application.find_all("stream"):
                stream_client = stream.find("client")
                stream_meta = stream.find("meta")
                stream_video = stream_meta.find("video")
                stream_audio = stream_meta.find("audio")

                stream_info[stream.find("name").text] = {
                    "time": stream.find("time").text,
                    "bw_audio": stream.find("bw_audio").text,
                    "bw_video": stream.find("bw_video").text,
                    "client": {
                        "time": stream_client.find("time").text,
                        "dropped": stream_client.find("dropped").text,
                        "avsync": stream_client.find("avsync").text,
                        "timestamp": stream_client.find("timestamp").text,
                    },
                    "meta": {
                        "video": {
                            "width": stream_video.find("width").text,
                            "height": stream_video.find("height").text,
                            "frame_rate": stream_video.find("frame_rate").text,
                            "codec": stream_video.find("codec").text,
                        },
                        "audio": {
                            "codec": stream_audio.find("codec").text,
                            "channels": stream_audio.find("channels").text,
                            "sample_rate": stream_audio.find("sample_rate").text,
                        },
                    },
                }

    print(stream_info)

    return stream_info
